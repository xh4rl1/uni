public void initialize(URL url, ResourceBundle rb) {
        
        try{
        
            AnchorPane vbox = FXMLLoader.load(getClass().getResource("/vista/VentanaDesplegable.fxml"));//Aqui va la ruta donde se encuentra la ventana a ddesplegar
            DrawfxId.setSidePane(vbox);
        }catch (IOException ex)
        {
            Logger.getLogger(VentanaDesplegableController.class.getName()).log(Level.SEVERE,null,ex);
        }
        
         HamburgerBackArrowBasicTransition transition = new     HamburgerBackArrowBasicTransition (HamburgerId);
         transition.setRate(-1);
         HamburgerId.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
         transition.setRate(transition.getRate() * - 1);
         transition.play();
         
         if(DrawfxId.isOpened())
         {
          DrawfxId.close();
         }else
         {
             DrawfxId.open();
         }
             
         }
                           
         );
    }    
    
    /*
    propiedades del text field
    en propiedades en style
    -fx-background-color -> transparente
    -fx-border-color -> El que quieras
    -fx-border-width ->  0px 0px 2px 0px
    
    */