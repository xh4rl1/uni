
import java.util.Stack;
import java.util.Vector;

public class DFS {

    public static BuscarPunto_GenerarHijos b_g = new BuscarPunto_GenerarHijos();

    public static Vector<Punto> busquedaPorProfundidad(Character[][] estadoJuego, Arbol raiz){

        Vector <Punto> salida = new Vector<Punto>();

        Arbol puntoActual = raiz;
        Stack<Arbol> busqueda = new Stack<Arbol>();
        busqueda.push(puntoActual);

        while(!busqueda.isEmpty() && !puntoActual.getmeEncontreAida()){
            busqueda.pop();
            //Generar los posibles hijos
            //¿Cuantos movimientos tiene el ratón?


            if(!puntoActual.getmeEncontreAlseñorDelasTinieblas() && !puntoActual.getmemeEncotreLagartoCongresista() && !puntoActual.getmeEncontreSeñoInmaduro() && !puntoActual.getmeEncontreunMuro()){

                //Izquierda
                if(puntoActual.getpuntoSeñorTocino().getY()>0){

                    Punto nuevoPunto = new Punto(puntoActual.getpuntoSeñorTocino().getX(),puntoActual.getpuntoSeñorTocino().getY()-1);

                    if(!b_g.buscarPunto(puntoActual.getRecorrido(), nuevoPunto)){
                        busqueda.add(b_g.generarHijo(nuevoPunto,puntoActual,estadoJuego));

                    }
                }


                //Derecha
                if(puntoActual.getpuntoSeñorTocino().getY()<estadoJuego[0].length-1){

                    Punto nuevoPunto = new Punto(puntoActual.getpuntoSeñorTocino().getX(),puntoActual.getpuntoSeñorTocino().getY()+1);

                    if(!b_g.buscarPunto(puntoActual.getRecorrido(), nuevoPunto)){
                        busqueda.add(b_g.generarHijo(nuevoPunto,puntoActual,estadoJuego));

                    }
                }


                //Arriba
                if(puntoActual.getpuntoSeñorTocino().getX()>0){

                    Punto nuevoPunto = new Punto(puntoActual.getpuntoSeñorTocino().getX()-1,puntoActual.getpuntoSeñorTocino().getY());

                    if(!b_g.buscarPunto(puntoActual.getRecorrido(), nuevoPunto)){
                        busqueda.add(b_g.generarHijo(nuevoPunto,puntoActual,estadoJuego));

                    }
                }


                //Abajo
                if(puntoActual.getpuntoSeñorTocino().getX()<estadoJuego.length-1){

                    Punto nuevoPunto = new Punto(puntoActual.getpuntoSeñorTocino().getX()+1,puntoActual.getpuntoSeñorTocino().getY());

                    if(!b_g.buscarPunto(puntoActual.getRecorrido(), nuevoPunto)){
                        busqueda.add(b_g.generarHijo(nuevoPunto,puntoActual,estadoJuego));

                    }
                }
            }

            puntoActual = busqueda.peek();
        }

        if(!busqueda.isEmpty()){
            salida = busqueda.peek().getRecorrido();
            salida.add(busqueda.peek().getpuntoSeñorTocino());
        }
        return salida;

    }


}
