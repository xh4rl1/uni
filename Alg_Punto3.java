package AlgoritmoRecursivo;

/**
 * Autores:Johan Alexis Franco, Luis Alfonso Berrio.
 *
 *  Solucion pregunta 2c R.R T(n) = 2T(n−1)−T(n−2) + 2 + 4n, T(0) = 16, T(1) = 48
 * Procedimiento Solucion Homogenia: r^(2)-2r+1= 0 raices -> (1,1) multiplicidad = 2
 * T(n) = A + Bn -> Solucion Homogenia
 * -----///---------------------------------------------------------------------
 * Procedimiento Solucion Particular: T(n) = (cn + d)*n^2
 * cn^3 + dn^2 Remplazamos: cn^(3)+dn^(2) = 2(c(n^(3)-3n^(2)+3n-1)+d(n^(2)-2n+1)-(c(n^(3)-6n^(2)+12n-8)+d(n^(2)-4n+4))+4n+2
 * Resolviendo nos da -> c=4/6 y d=3
 * Solucion total= T(n) = A +bn +4/6n^(3) + 3n^(2)
 * Aplicando las condiciones iniciales tenemos A = 16 y B = 85/3
 * -> T(n) = 16 + 85/3n + 4/6^n(3) + 3n^(2)
 * Aplicando algebra 16 + n(85/3 + 4/6n^(2) + 3n)
 * Factorizando y sacando factor comun -> 16 + 1/3n(n(n(2n+9)+85)
 *
 * Acomodando nos daria -> 1/3n(n(2n + 9)+85)+16*/

public class Alg_Punto3 {

    public  static long algoritmoRecursivo(int n)
    {
        if(n == 0)return 16;/** Condicion inicial T(0) = 16*/
        if(n == 1) return 48;/** Condicion incial T(1) = 48*/
        else return 2 * algoritmoRecursivo(n - 1) - algoritmoRecursivo(n - 2) + 2 + 4*n; /** T(n) = 2T(n−1)−T(n−2) + 2 + 4n*/
    }

    public  static  long solucioNumerica(int n)
    {
         return  (long) (n *( n * ( 2 * n + 9 ) + 85 ) / 3) + 16;/** 1/3n(n(2n + 9)+85)+16*/
    }

    public static void main(String[] args) {
        for (int i = 0;i < 10; i++)
            System.out.printf("(n) -> %d recursivo: %d solucion: %d\n",i,algoritmoRecursivo(i),solucioNumerica(i));
    }
}
