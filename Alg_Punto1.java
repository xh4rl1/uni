package AlgoritmoRecursivo;

public class Alg_Punto1 {

    public static long algoritmoRecursivo(int n)
    {
        if(n == 1)return 2;
        else{
             final long m = 2;
             return m + 4*algoritmoRecursivo((int) Math.floor((n/2)));
        }
    }

    public  static long log2(int x){
        long res =  (long) (Math.log(x)/Math.log(2));
        return res;
    }

    public static long solucionNumerica(int n) {
        return (long) ((8 * Math.pow(n,log2(4))) - 2)/3;
    }


    public static void main(String[] args) {
        for (int i = 2; i < 100;i*=2)
            System.out.printf("(n) -> %d recursivo: %d Solucion: %d\n",i,algoritmoRecursivo(i),solucionNumerica(i));

    }
}
