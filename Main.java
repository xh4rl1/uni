
import java.util.Scanner;
import java.util.Vector;

public class main {


    public static void main(String[] args) {
        CargarArchivo a = new CargarArchivo();
        BFS b = new BFS();
        DFS d = new DFS();
        Vector<Punto> recorrido = new Vector<Punto>();
        String t = a.abrirArchivo();
        String m = t.replace(" ", "");
        Character juego[][] = a.verificarArchivo(m);
        int x = 0, y = 0, fila = juego.length, columna = juego[0].length, opcion = 0;

        //obtengo la posición inicial del ratón
        for (int i = 0; i < fila; i++) {
            for (int j = 0; j < columna; j++) {
                if (juego[i][j] == '1') {
                    x = i;
                    y = j;
                }
            }
        }

        Arbol raiz = new Arbol(new Punto(x, y), recorrido, false, false, false, false, false);
        Scanner input = new Scanner(System.in);
        Vector<Punto> busquedaAmplitud = b.busquedaPorAmplitud(juego, raiz);
        Vector<Punto> busquedaProfundidad = d.busquedaPorProfundidad(juego, raiz);

        System.out.print("Igrese la busqueda.\n1.)DFS\n2.)BFS\nopcion -> ");
        opcion = input.nextInt();

        switch (opcion) {
            case 1:
                for (int i = 0; i < busquedaProfundidad.size(); i++) {
                    System.out.println(String.valueOf(busquedaProfundidad.get(i).getX() + 1) + " " + String.valueOf(busquedaProfundidad.get(i).getY() + 1));
                }
                break;
            case 2:
                for (int i = 0; i < busquedaAmplitud.size(); i++) {
                    System.out.println(String.valueOf(busquedaAmplitud.get(i).getX() + 1) + " " + String.valueOf(busquedaAmplitud.get(i).getY() + 1));
                }
                break;
        }
    }
}