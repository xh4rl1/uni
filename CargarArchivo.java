import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CargarArchivo extends Component {

    public Character[][] verificarArchivo(String t){
        int n = Integer.parseInt( ""+ t.charAt(0));
        int piv = 0, aux = 0,fila,columna;
        Character[][] matrizAdyacencia = new Character[n][n];
        fila = matrizAdyacencia.length;
        columna = matrizAdyacencia[0].length;

        for (int i=0; i < fila;i++){
            for (int j = 0; j < columna;j++){
                piv = 2 + j + aux;
                matrizAdyacencia[i][j] = t.charAt(piv);
            }
            aux = piv;
        }
        return matrizAdyacencia;
    }

    //funcion para abrir el archivo y leer su contenido.
    public String abrirArchivo(){
        String almacena = "";
        String text = "";
        int n = 0;
        try {
            JFileChooser file = new JFileChooser();
            file.showOpenDialog(this);
            File open = file.getSelectedFile();

            //leemos el archivo
            if (open!=null){
                FileReader archiv = new FileReader(open);
                BufferedReader read = new BufferedReader(archiv);
                while ((almacena = read.readLine())!=null) {
                    text += almacena + "\n";
                }
                read.close();
            }

        }catch (IOException ex){
            JOptionPane.showMessageDialog(null,ex + "" + "\nNo se ha encontrado el archivo", "ADVERTENCIA!!!",JOptionPane.WARNING_MESSAGE);
        }
        return text;
    }

}