import java.util.Vector;

public class BuscarPunto_GenerarHijos {

    public static boolean buscarPunto(Vector<Punto> recorrido, Punto pnt){

        for(int i=0; i<recorrido.size(); i++){
            if(recorrido.get(i).getX() == pnt.getX() && recorrido.get(i).getY()==pnt.getY()){
                return true;
            }

        }
        return false;
    }

    public static Arbol generarHijo(Punto nuevoPunto, Arbol puntoActual, Character[][] estadoJuego){

        Vector<Punto> recorridoNuevo = new Vector<Punto>();

        for(int i=0; i<puntoActual.getRecorrido().size(); i++){
            recorridoNuevo.add(puntoActual.getRecorrido().get(i));
        }

        recorridoNuevo.add(puntoActual.getpuntoSeñorTocino());
        boolean meEncontreAlseñorDelasTinieblas = false;
        boolean meEncontreAida = false;
        boolean meEncotreLagartoCongresista = false;
        boolean meEncontreSeñoInmaduro = false;
        boolean meEncontreunMuro = false;

        if(estadoJuego[nuevoPunto.getX()][nuevoPunto.getY()]=='5'){
            meEncontreAlseñorDelasTinieblas = true;
        }

        else if(estadoJuego[nuevoPunto.getX()][nuevoPunto.getY()]=='2'){
            meEncontreAida = true;
        }

        else if(estadoJuego[nuevoPunto.getX()][nuevoPunto.getY()]=='4'){
            meEncotreLagartoCongresista = true;
        }

        else if(estadoJuego[nuevoPunto.getX()][nuevoPunto.getY()]=='6'){
            meEncontreSeñoInmaduro = true;
        }
        else if(estadoJuego[nuevoPunto.getX()][nuevoPunto.getY()]=='3'){
            meEncontreunMuro = true;
        }

        Arbol nuevoHijo = new Arbol(nuevoPunto,recorridoNuevo,meEncotreLagartoCongresista,meEncontreAlseñorDelasTinieblas,meEncontreAida,meEncontreSeñoInmaduro,meEncontreunMuro);
        return nuevoHijo;

    }
}
