
import java.util.Arrays;

/**
 * La matriz debe ser cargada con el formato de graph online debe seguir la siguiente plantilla
 *
 * numeroVertices -> No se permite en el archivo txt dejar espacios en el numero de vertices
 * 0 0 1  . . .
 * 1 1. . .0 0 0
 * . . .  1 1
 *
 * ej:
 * 4" " -> no puede tener un espacio en blanco esto generara un fallo solo se permite saltos de linea
 * 0 0 1  . . .
 * 1 1. . .0 0 0
 * . . .  1 1
 *
 *Nota la matriz de graph online tiene la forma
 * 0, 1, 0, 1, 1,
 * 1, 0, 1, 1, 0,
 * 0, 1, 0, 1, 1,
 * 1, 1, 1, 0, 1,
 * 1, 0, 1, 1, 0,
 *
 * El programa lo lleva a una forma estandar
 * Forma estandar.
 *
 *  01011
 *  10110
 *  01011
 *  11101
 *  10110
 *
 * */

public class Principal {

    //funcion principal.
    public static void main(String[] args) {
        abrirArchivo open = new abrirArchivo();
        String t  = open.abrirArchivo();
        String m = t.replace(", ",""); //limpiamos la matriz para llevarlo a un formato estandar
        Character[][] salida = open.verificarArchivo(m);
        for (int i = 0; i < salida.length;i++){System.out.println(Arrays.toString(salida[i]));}
    }
}
