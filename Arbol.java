
import java.util.*;

public class Arbol {

        private Punto puntoSeñorTocino;
        private Vector<Punto> recorridos;
        private boolean meEncontreAlseñorDelasTinieblas;
        private boolean meEncontreAida;
        private boolean meEncotreLagartoCongresista;
        private boolean meEncontreSeñoInmaduro;
        private boolean meEncontreunMuro;

        Arbol(Punto puntoSeñorTocino, Vector<Punto> recorridos, boolean meEncotreLagartoCongresista, boolean meEncontreAlseñorDelasTinieblas,boolean meEncontreAida, boolean meEncontreSeñoInmaduro,boolean meEncontreunMuro){
            this.puntoSeñorTocino = puntoSeñorTocino;
            this.recorridos = recorridos;
            this.meEncontreAida = meEncontreAida;
            this.meEncontreAlseñorDelasTinieblas = meEncontreAlseñorDelasTinieblas;
            this.meEncontreSeñoInmaduro = meEncontreSeñoInmaduro;
            this.meEncotreLagartoCongresista = meEncotreLagartoCongresista;
            this.meEncontreunMuro = meEncontreunMuro;
        }

        public Punto getpuntoSeñorTocino(){return puntoSeñorTocino;}
        public Vector<Punto> getRecorrido(){return recorridos; }

        //Condiciones en que el juego paro
        public boolean getmeEncontreAida(){return meEncontreAida;}
        public boolean getmeEncontreAlseñorDelasTinieblas(){return meEncontreAlseñorDelasTinieblas;}
        public boolean getmeEncontreSeñoInmaduro(){return meEncontreSeñoInmaduro;}
        public boolean getmemeEncotreLagartoCongresista(){return meEncotreLagartoCongresista;}
        public boolean getmeEncontreunMuro(){return  meEncontreunMuro;}
}
